<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Inspeksi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_inspeksi');
    }

    public function tambah_inspeksi($id_pesanan)
    {
        $kategori_id = $this->uri->segment(4) ? $this->uri->segment(4) : 1; // 1 adalah langkah pertama dalam form yaitu kategori dengan id 1
        $form_kategori = $this->m_inspeksi->kategoriById($kategori_id);
        $form_item = $this->m_inspeksi->itemByKategori($form_kategori->id);

        // update feature
        $inserted_data = $this->m_inspeksi->detailInspeksiByPesananAndKategori($id_pesanan, $kategori_id);
        // echo "<pre>";print_r($inserted_data);exit();
        $inserted_catatan_kategori = $this->m_inspeksi->getCatatanKategoriByPesananAndKategori($id_pesanan, $kategori_id);
        // echo "<pre>";print_r($inserted_catatan_kategori);exit();
        // echo "<pre>";print_r($this->m_inspeksi->getMaxKategori());exit();

        $data = array(
            'id_pesanan' => $id_pesanan,
            'kategori_id' => $kategori_id,
            'kategori' => $form_kategori,
            'item' => $form_item,
            'updated_item' => $inserted_data,
            'updated_catatan' => $inserted_catatan_kategori,
            'max_kat' => $this->m_inspeksi->getMaxKategori()->max_kat
        );
        $this->load->view('tambah_inspeksi', $data);
    }

    public function save_inspeksi()
    {
        $id_pesanan = $this->input->post('id_pesanan');
        $id_kategori = $this->input->post('id_kategori');
        $all_param = array();
        foreach($this->input->post('item') as $i){
            $all_param[] = array(
                "id_pesanan" => $id_pesanan,
                "id_item" => $i,
                "status"=> $this->input->post('status-'.$i),
                "catatan_per_item" => $this->input->post('catatan-'.$i) ? $this->input->post('catatan-'.$i) : null,
                "created_date" => date('Y-m-d h:i:s')
            );
        }
        // save inspeksi detail
        $save = $this->m_inspeksi->saveInspeksi($all_param);

        // save catatan perkategori *optional
        if($this->input->post('catatan')){
            $paramSaveKategori = array(
                "id_pesanan" => $id_pesanan,
                "id_kategori" => $id_kategori,
                "catatan" => $this->input->post('catatan')
            );
            $this->m_inspeksi->saveCatatanKategori($paramSaveKategori);
        }
        
        if($save){
            $next = $id_kategori + 1;    
            if($next == $this->m_inspeksi->getMaxKategori()->max_kat){
                redirect('pesanan/detail/' . $id_pesanan);
            }else{
                redirect('inspeksi/tambah_inspeksi/' . $id_pesanan . '/' . $next);
            }
        }
    }

    public function update_inspeksi()
    {
        //perform update bulk
        $id_pesanan = $this->input->post('id_pesanan');
        $id_kategori = $this->input->post('id_kategori');
        $all_param = array();
        foreach ($this->input->post('id_detail') as $detil) {
            $all_param[] = array(
                "id" => $detil,
                "id_pesanan" => $id_pesanan,
                "id_item" => $this->input->post('item-'.$detil),
                "status" => $this->input->post('status-'.$detil),
                "catatan_per_item" => $this->input->post('catatan-'.$detil) ? $this->input->post('catatan-'.$detil) : null,
                "edited_date" => date('Y-m-d h:i:s'),
            );
        }


        $update_bulk = $this->m_inspeksi->updateInspeksi($all_param);
        //update catatan per kategori
        if ($this->input->post('catatan')) {
            // cek apakah udah ada catatan?
            // jika ada maka update
            // jika tidak maka insert
            $cek = $this->m_inspeksi->getCatatanKategoriByPesananAndKategori($id_pesanan, $id_kategori);
            if($cek){
                //update
                $paramUpdateCatatanKategori = array(
                    'catatan' =>$this->input->post('catatan')
                );
                $this->m_inspeksi->updateCatatanByPesananAndKategori($id_pesanan, $id_kategori, $paramUpdateCatatanKategori);
            }else{
                // insert
                $paramCatatanKategori = array(
                    "id_pesanan" => $id_pesanan,
                    "id_kategori" => $id_kategori,
                    "catatan" => $this->input->post('catatan'),
                );
                $this->m_inspeksi->saveCatatanKategori($paramCatatanKategori);
            }   
            
        }

        if($update_bulk){
            $next = $id_kategori + 1;
            if ($next == $this->m_inspeksi->getMaxKategori()->max_kat) {
                redirect('pesanan/detail/' . $id_pesanan);
            } else {
                redirect('inspeksi/tambah_inspeksi/' . $id_pesanan . '/' . $next);
            }
        }

        
    }

    public function tambah_foto($id_pesanan)
    {
        $this->load->helper(array('file'));
        $this->load->view('upload_foto',array('id_pesan'=>$id_pesanan));
    }

    public function do_upload()
    {
        $dir = 'pesanan-'.$this->uri->segment(3);
        
        if(!is_dir(FCPATH . '/upload/'.$dir)){
            mkdir(FCPATH . '/upload/' . $dir, 0777, true);
        }
        $config['upload_path'] = FCPATH . '/upload/'.$dir;
        $config['allowed_types'] = 'gif|jpg|png|ico';
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('fotoInspeksi')) {
            $nama = $this->upload->data('file_name');
            $this->m_inspeksi->insertFotoInspeksi(array(
                'id_pesanan'=>$this->uri->segment(3),
                'filename'=>$nama,
                'uploaded_at'=>date('Y-m-d h:i:s')
            ));
        }

    }

    public function edit_upload($id_pesanan)
    {
        if(count($this->m_inspeksi->getFotoInspeksiByPesanan($id_pesanan))==0){
            return redirect('inspeksi/tambah_foto/'.$id_pesanan);
        }
        $uploadan = $this->m_inspeksi->getFotoInspeksiByPesanan($id_pesanan);
        $this->load->view('edit_upload',array(
            'uploadan'=>$uploadan,
            'id_pesanan'=>$id_pesanan
        ));
    }

    public function remove_foto()
    {

    }

    public function update_foto_inspeksi()
    {
        $id_pesanan = $this->input->post('id_pesanan');
        $folder = "pesanan-".$id_pesanan;
        chdir('upload/'.$folder);
        $all_param = array();
        foreach($this->input->post('id_upload') as $i){
            //get ekstensi
            $old_filename = $this->input->post('filename-hidden-'.$i);
            $exp_filename = explode(".", $old_filename);
            
            $new_filename = $this->input->post('filename-'.$i).".".$exp_filename[1];
            $all_param[] = array(
                "id"=>$i,
                "filename"=> $new_filename,
                "deskripsi"=>$this->input->post('deskripsi-'.$i)
            );
            //rename file in directori
            rename($old_filename, $new_filename);
        }
        $exe = $this->m_inspeksi->updateFoto($all_param);
        redirect('pesanan/detail/' . $id_pesanan);

    }
}

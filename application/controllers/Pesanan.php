<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pesanan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->load->model('m_pesanan');
        $this->load->model('m_inspeksi');
    }
    public function index()
    {
        $this->load->view('pesanan', array(
            'pesanan' => $this->m_pesanan->getAllPesanan()
        ));
    }

    public function tambah_pesanan()
    {
        $this->load->view('tambah_pesanan', array(
            "pelanggan" => $this->m_pesanan->allPelanggan()
        ));
    }

    public function insert_pesanan()
    {
        $post = array(
            "id_pelanggan" => $this->input->post("pelanggan"),
            "merk" => $this->input->post("merk"),
            "warna" => $this->input->post("warna"),
            "model" => $this->input->post("model"),
            "no_polisi" => $this->input->post("nopol"),
            "tahun" => $this->input->post("tahun"),
            "no_rangka" => $this->input->post("norangka"),
            "no_mesin" => $this->input->post("nomesin"),
            "odometer" => $this->input->post("odometer"),
            "pajak_hingga" => $this->input->post("pajak"),
            "alamat_inspeksi" => $this->input->post("alamat"),
            "created_by" => "admin tengokin",
            "created_date" => date('Y-m-d H:i:s'),
        );

        $exe = $this->m_pesanan->addPesanan($post);

        if($exe){
            redirect('pesanan');
        }else{
            echo "upss";
        }
    }

    public function detail()
    {
        if(!$this->uri->segment(3)){
            redirect('pesanan');
        }
        $detail = $this->m_pesanan->detailPesanan($this->uri->segment(3));

        //detail inspeksi
        $detail_inspeksi = $this->m_inspeksi->detailInspeksi($this->uri->segment(3));
        // echo "<pre>";print_r($detail_inspeksi);exit();

        $group = array();
        foreach($detail_inspeksi as $d){
            $group[$d->kategori_id]['id_kategori'] = $d->kategori_id;
            $group[$d->kategori_id]['nama_kategori'] = $d->kategori_nama;
            $group[$d->kategori_id]['catatan_kategori'] = $d->kat_catatan;
            $group[$d->kategori_id]['item'][] = $d;
        }
        // echo "<pre>";print_r($group);exit();

        $this->load->view('detail_pesanan', array(
            'detail' => $detail,
            'detail_inpeksi' => $group
        ));
    }
}

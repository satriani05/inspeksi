<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_inspeksi extends CI_Model
{
    public function getAllKategori()
    {
        return $this->db->get('kategori')->result();
    }

    public function kategoriById($id)
    {
        return $this->db->get_where('kategori', array('id'=>$id))->row();
    }

    public function itemByKategori($kat_id)
    {
        return $this->db->get_where('item', array('id_kategori' => $kat_id))->result();
    }

    public function saveInspeksi($data)
    {
        return $this->db->insert_batch('pesanan_detail', $data);
    }

    public function saveCatatanKategori($data)
    {
        return $this->db->insert('pesanan_catatan', $data);
    }

    public function detailInspeksi($id_pesanan)
    {
        $sql = $this->db->select('a.id AS id_detil, a.status, a.catatan_per_item,b.nama AS item_nama,c.id AS kategori_id,c.nama AS kategori_nama,d.catatan AS kat_catatan')
                ->from('pesanan_detail a')
                ->join('item b','a.id_item = b.id', 'left')
                ->join('kategori c', 'b.id_kategori = c.id', 'left')
                ->join('pesanan_catatan d', 'd.id_kategori = c.id', 'left')
                ->where('a.id_pesanan', $id_pesanan)
                ->get();
        return $sql->result();
    }

    public function detailInspeksiByPesananAndKategori($id_pesanan, $id_kategori)
    {
        $sql = $this->db->select('a.id AS id_detil,
                                a.*,
                                b.id AS id_item,
                                b.nama AS item_nama,
                                c.id  AS id_kategori*')
                ->from('pesanan_detail a')
                ->join('item b','a.id_item = b.id', 'left')
                ->join('kategori c', 'b.id_kategori = c.id', 'left')
                ->where('a.id_pesanan', $id_pesanan)
                ->where('c.id', $id_kategori)
                ->get();
        return $sql->result();
    }

    public function getCatatanKategoriByPesananAndKategori($id_pes, $kat_id)
    {
        return $this->db->get_where('pesanan_catatan', array('id_pesanan'=>$id_pes, 'id_kategori'=>$kat_id))->row();
    }

    public function updateInspeksi($data)
    {
        // perform update bulk
        return $this->db->update_batch('pesanan_detail', $data, 'id');
    }

    public function updateCatatanByPesananAndKategori($id_pes, $kat_id, $data)
    {
        $this->db->where('id_pesanan',$id_pes);
        $this->db->where('id_kategori',$kat_id);
        return $this->db->update('pesanan_catatan', $data);
    }

    public function getMaxKategori()
    {
        $this->db->select_max('id','max_kat');
        return $this->db->get('kategori')->row();
    }

    public function insertFotoInspeksiBatch($data)
    {
        return $this->db->insert_batch('foto_inspeksi', $data);
    }

    public function insertFotoInspeksi($data)
    {
        return $this->db->insert('foto_inspeksi', $data);
    }

    public function getFotoInspeksiByPesanan($id_pesan)
    {
        return $this->db->get_where('foto_inspeksi', array('id_pesanan'=>$id_pesan))->result();
    }

    public function updateFoto($data)
    {
        // perform update bulk
        return $this->db->update_batch('foto_inspeksi', $data, 'id');
    }
}

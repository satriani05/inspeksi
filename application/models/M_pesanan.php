<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pesanan extends CI_Model 
{
    public function getAllPesanan()
    {
        $sql = $this->db->select('a.*, a.id as id_pesanan, b.*')->from('pesanan a')
                ->join('pelanggan b','a.id_pelanggan = b.id', 'left')
                ->get();
        return $sql->result();
    }

    public function detailPesanan($id)
    {
        $sql = $this->db->select('a.*, a.id as id_pesanan, b.*')->from('pesanan a')
            ->join('pelanggan b', 'a.id_pelanggan = b.id', 'left')
            ->where('a.id', $id)
            ->get();
        return $sql->row();

    }

    public function allPelanggan()
    {
        return $this->db->get('pelanggan')->result();
    }

    public function addPesanan($data)
    {
        return $this->db->insert('pesanan', $data);
    }
}

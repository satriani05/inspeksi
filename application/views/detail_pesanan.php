<?php $this->load->view('partials/header'); ?>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h4>Detail Pesanan</h4>
        
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-4">
            <img src="<?php echo base_url();?>assets/tesla.jpg" alt="" class="img-thumbnail">
          </div>
          <div class="col-lg-8">
            <div class="table-responsive">
              <table class="table table-hover">
                <tr>
                  <td>Pemesanan</td>
                  <td>:</td>
                  <td><b><?php echo $detail->nama;?></b></td>
                </tr>
                <tr>
                  <td>Tanggal Inspeksi</td>
                  <td>:</td>
                  <td><b><?php echo $detail->tgl_pesanan;?></b></td>
                </tr>
                <tr>
                  <td>Merk</td>
                  <td>:</td>
                  <td><b><?php echo $detail->merk;?></b></td>
                </tr>
                <tr>
                  <td>Model</td>
                  <td>:</td>
                  <td><b><?php echo $detail->model;?></b></td>
                </tr>
                <tr>
                  <td>Transmisi</td>
                  <td>:</td>
                  <td></td>
                </tr>
                <tr>
                  <td>Tahun</td>
                  <td>:</td>
                  <td><b><?php echo $detail->tahun;?></b></td>
                </tr>
                <tr>
                  <td>No Rangka</td>
                  <td>:</td>
                  <td><b><?php echo $detail->no_rangka;?></b></td>
                </tr>
                <tr>
                  <td>No Mesin</td>
                  <td>:</td>
                  <td><b><?php echo $detail->no_mesin;?></b></td>
                </tr>
                <tr>
                  <td>Odometer</td>
                  <td>:</td>
                  <td><b><?php echo $detail->odometer;?></b></td>
                </tr>
                <tr>
                  <td>No Polisi</td>
                  <td>:</td>
                  <td><b><?php echo $detail->no_polisi;?></b></td>
                </tr>
                <tr>
                  <td>Pajak Hingga</td>
                  <td>:</td>
                  <td><b><?php echo $detail->pajak_hingga;?></b></td>
                </tr>
              </table>
              <a class="btn btn-warning btn-lg" href="<?php echo site_url('inspeksi/tambah_inspeksi/'.$detail->id_pesanan);?>">Inspeksi</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- detail hasil inspeksi -->
<?php if($detail_inpeksi){?>
<div class="row">
  <div class="col-lg-12">
    <?php foreach($detail_inpeksi as $i):?>
      <div class="panel panel-success">
        <div class="panel-heading">
          <h4>Hasil Inspeksi <?php echo $i['nama_kategori'];?></h4>
        </div>
        <div class="panel-body">
          <?php if($i['catatan_kategori']){ ?>
          <small>catatan: </small>
          <blockquote>
            <p><?php echo $i['catatan_kategori'];?></p>
          </blockquote>
          <?php } ?>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Item</th>
                  <th>Status</th>
                  <th>Catatan</th>
                </tr>
              </thead>
              <tbody>
                <?php $no =1; foreach($i['item'] as $d):?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $d->item_nama; ?></td>
                  <td>
                    <?php 
                      if($d->status == 0){
                        echo '<b class="text-danger">N/A</b>';
                      }else if($d->status == 1){
                        echo '<b class="text-success">OK</b>';
                      }else{
                        echo '<b class="text-warning">TROUBLE</b>';
                      }
                    ?>
                  </td>
                  <td><?php echo $d->catatan_per_item ? $d->catatan_per_item : '-'; ?></td>
                </tr>
                <?php $no++; endforeach;?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    <?php endforeach;?>
  </div>
</div>
<?php }?>
<?php $this->load->view('partials/footer'); ?>
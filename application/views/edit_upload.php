<?php $this->load->view('partials/header');?>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h4>Edit Foto Inspeksi</h4>
      </div>
      <form method="post" action="<?php echo site_url('inspeksi/update_foto_inspeksi');?>">
      <input type="hidden" value="<?php echo $this->uri->segment(3);?>" name="id_pesanan">
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-12">
            <div class="table-responsive">
              <table class="table table-stripped">
                <?php foreach($uploadan as $u):?>
                <tr>
                  <td>
                    <img src="<?php echo base_url(); ?>upload/pesanan-<?php echo $id_pesanan;?>/<?php echo $u->filename;?>" width="400" height="400" class="img-responsive img-rounded">
                    <input type="hidden" name="id_upload[]" value="<?php echo $u->id;?>">
                  </td>
                  <td>
                    <div class="form-group">
                      <input type="hidden" value="<?php echo $u->filename;?>"  name="filename-hidden-<?php echo $u->id;?>">
                      <input type="text" class="form-control" value="<?php echo $u->filename;?>" placeholder="filename" name="filename-<?php echo $u->id;?>">
                    </div>
                    <div class="form-group">
                      <textarea name="deskripsi-<?php echo $u->id;?>" placeholder="deskripsi" class="form-control" cols="30" rows="10"><?php echo $u->deskripsi;?></textarea>
                    </div>
                  </td>
                </tr>
                <?php endforeach;?> 
              </table>
            </div>
          </div>
          <div class="col-lg-12">
            <input type="submit" value="edit" class="btn btn-lg btn-success pull-right">
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>
<?php $this->load->view('partials/footer');

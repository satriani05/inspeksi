<?php $this->load->view('partials/header'); ?>
<div class="row">
  <a href="<?php echo site_url('pesanan/tambah_pesanan');?>" class="btn btn-primary pull-right">tambah inspeksi</a>
  <h1>List Pesanan</h1>
  <hr>
  <div class="col-lg-12">
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <th>No</th>
          <th>Pelanggan</th>
          <th>Tgl Pesan</th>
          <th>Merk</th>
          <th>Model</th>
          <th>Warna</th>
          <th>No Polisi</th>
          <th>#Action</th>
        </thead>
        <tbody>
          <?php $no  = 1; foreach($pesanan as $p):?>
            <tr>
              <td><?php echo $no;?></td>
              <td><?php echo $p->nama;?></td>
              <td><?php echo $p->tgl_pesanan;?></td>
              <td><?php echo $p->merk;?></td>
              <td><?php echo $p->model;?></td>
              <td><?php echo $p->warna;?></td>
              <td><?php echo $p->no_polisi;?></td>
              <td>
                <a class="btn btn-success" href="<?php echo site_url('pesanan/detail/'.$p->id_pesanan);?>">Detail</a>
                <br>
                <br>
                <a class="btn btn-info" href="<?php echo site_url('inspeksi/tambah_inspeksi/'.$p->id_pesanan);?>">Inspeksi</a>
              </td>
            </tr>
          <?php $no++; endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<?php $this->load->view('partials/footer');

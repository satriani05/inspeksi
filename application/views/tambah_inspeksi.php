<?php $this->load->view('partials/header');?>
<?php 
// echo "<pre>";
// var_dump($kategori);
// exit();
?>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h4>Form <?php echo $kategori_id; ?> : <?php echo $kategori->nama;?></h4>
      </div>
      <?php if(count($updated_item)>0){ ?>
        <div class="panel-body">
          <form action="<?php echo site_url('inspeksi/update_inspeksi');?>" method="post">
          <input type="hidden" name="id_pesanan" value="<?php echo $id_pesanan;?>">
          <input type="hidden" name="id_kategori" value="<?php echo $kategori_id; ?>">
          <?php foreach($updated_item as $u):?>
          <div class="row">  
            <div class="col-lg-12">
              <div class="row">
                <div class="col-lg-4">
                  <div class="form-group">
                    <label for=""><?php echo $u->item_nama;?></label>
                    <input type="hidden" name="id_detail[]" value="<?php echo $u->id_detil; ?>">
                    <input type="hidden" name="item-<?php echo $u->id_detil; ?>" value="<?php echo $u->id_item; ?>">
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="form-group">
                    
                    <?php if($u->status == 0){?>
                      <label class="radio-inline">
                        <input type="radio" name="status-<?php echo $u->id_detil; ?>" value="0" checked="checked">N/A
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="status-<?php echo $u->id_detil; ?>" value="1" required >OK
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="status-<?php echo $u->id_detil; ?>" value="2" required >TROUBLE
                      </label>
                    <?php }?>

                    <?php if($u->status == 1){?>
                      <label class="radio-inline">
                        <input type="radio" name="status-<?php echo $u->id_detil; ?>" value="0" required >N/A
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="status-<?php echo $u->id_detil; ?>" value="1" required checked="checked">OK
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="status-<?php echo $u->id_detil; ?>" value="2" required >TROUBLE
                      </label>
                    <?php }?>

                    <?php if($u->status == 2){?>
                      <label class="radio-inline">
                        <input type="radio" name="status-<?php echo $u->id_detil; ?>" value="0" required >N/A
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="status-<?php echo $u->id_detil; ?>" value="1" required >OK
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="status-<?php echo $u->id_detil; ?>" value="2" required checked="checked">TROUBLE
                      </label>
                    <?php }?>
                    
                    
    
                  </div>
                  <?php if($kategori->setting_catatan==1):?>
                  <div class="form-group">
                    <input type="text" class="form-control" name="catatan-<?php echo $u->id_detil;?>" placeholder="Catatan inspeksi" value="<?php echo $u->catatan_per_item;?>">
                  </div>
                  <?php endif;?>
                </div>
              </div>
            </div>
          </div>
          <?php endforeach;?>
          <hr>
          <div class="form-group">
            <label for="">Catatan Hasil Inspeksi <?php echo $kategori->nama;?></label>
            <textarea name="catatan" class="form-control" placeholder="catatan hasil inspeksi"><?php echo $updated_catatan ? $updated_catatan->catatan : ''; ?></textarea>
          </div>
          <?php if ($kategori_id > 1) {?>
            <?php $prev = $kategori_id - 1;?>
            <a class="btn btn-warning" href="<?php echo site_url('inspeksi/tambah_inspeksi/'.$id_pesanan.'/'.$prev); ?>">Back</a>
          <?php }?>
          <?php if ($kategori_id == $max_kat) {?>
            <!-- tambahkan update foto inspeksi sebelum finish -->
            <a class="btn btn-info" href="<?php echo site_url('inspeksi/tambah_foto/'.$id_pesanan.'/upload-foto'); ?>">Goto Foto Ispeksi</a>
            <!-- <button type="submit" class="btn btn-success">Finish</button> -->
          <?php }else if($this->uri->segment(4)=='upload-foto'){?>
            <button type="submit" class="btn btn-success">Finish</button>  
          <?php } else {?>
            <button type="submit" class="btn btn-success">Update and Next</button>
          <?php }?>
          </form>
        </div>
      <?php }else{ ?>
        <div class="panel-body">
          <form action="<?php echo site_url('inspeksi/save_inspeksi');?>" method="post">
          <?php foreach($item as $i):?>
          <div class="row">
            <input type="hidden" name="id_pesanan" value="<?php echo $id_pesanan;?>">
            <input type="hidden" name="id_kategori" value="<?php echo $kategori_id; ?>">
            <div class="col-lg-12">
              <div class="row">
                <div class="col-lg-4">
                  <div class="form-group">
                    <label for=""><?php echo $i->nama;?></label>
                    <input type="hidden" name="item[]" value="<?php echo $i->id; ?>">
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="form-group">
                    <label class="radio-inline">
                      <input type="radio" name="status-<?php echo $i->id; ?>" value="0" required>N/A
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="status-<?php echo $i->id; ?>" value="1" required>OK
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="status-<?php echo $i->id; ?>" value="2" required>TROUBLE
                    </label>
                  </div>
                  <?php if($kategori->setting_catatan==1):?>
                  <div class="form-group">
                    <input type="text" class="form-control" name="catatan-<?php echo $i->id;?>" placeholder="Catatan inspeksi">
                  </div>
                  <?php endif;?>
                </div>
              </div>
            </div>
          </div>
          <?php endforeach;?>
          <hr>
          <div class="form-group">
            <label for="">Catatan Hasil Inspeksi <?php echo $kategori->nama;?></label>
            <textarea name="catatan" class="form-control" placeholder="catatan hasil inspeksi"></textarea>
          </div>
          <?php if ($kategori_id > 1) {?>
            <?php $prev = $kategori_id - 1;?>
            <a class="btn btn-warning" href="<?php echo site_url('inspeksi/tambah_inspeksi/'.$id_pesanan.'/'.$prev); ?>">Back</a>
          <?php }?>
          <?php if($kategori_id == $max_kat){?>
            <!-- <button type="submit" class="btn btn-success">Finish</button> -->
            <a class="btn btn-info" href="<?php echo site_url('inspeksi/tambah_foto/'.$id_pesanan.'/upload-foto'); ?>">Goto Foto Ispeksi</a>
          <?php }else if($this->uri->segment(4)=='upload-foto'){?>
            <button type="submit" class="btn btn-success">Finish</button> 
          <?php }else{ ?>
            <button type="submit" class="btn btn-success">Save and Next</button>
          <?php } ?>
          
          </form>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
<?php $this->load->view('partials/footer');?>
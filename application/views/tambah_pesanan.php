<?php $this->load->view('partials/header'); ?>
<div class="row">
    <div class="col-lg-6">
        <h1>Tambah Pesanan</h1>
        <form method="post" action="<?php echo site_url('pesanan/insert_pesanan');?>">
            <div class="form-group">
                <label for="">Pelanggan</label>
                <select name="pelanggan" class="form-control">
                    <?php foreach($pelanggan as $p):?>
                    <option value="<?php echo $p->id;?>"><?php echo $p->nama;?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="form-group">
                <label for="">Merk Mobil</label>
                <input type="text" class="form-control" name="merk" id="" placeholder="..." required="true">
            </div>
            <div class="form-group">
                <label for="">Model</label>
                <input type="text" class="form-control" name="model" id="" placeholder="..." required="true">
            </div>
            <div class="form-group">
                <label for="">Warna</label>
                <input type="text" class="form-control" name="warna" id="" placeholder="..." required="true">
            </div>
            <div class="form-group">
                <label for="">No Polisi</label>
                <input type="text" class="form-control" name="nopol" id="" placeholder="..." required="true">
            </div>
            <div class="form-group">
                <label for="">Tahun</label>
                <input type="text" class="form-control" name="tahun" id="" placeholder="..." required="true">
            </div>
            <div class="form-group">
                <label for="">No Rangka</label>
                <input type="text" class="form-control" name="norangka" id="" placeholder="..." required="true">
            </div>
            <div class="form-group">
                <label for="">No Mesin</label>
                <input type="text" class="form-control" name="nomesin" id="" placeholder="..." required="true">
            </div>
            <div class="form-group">
                <label for="">Odometer</label>
                <input type="text" class="form-control" name="odometer" id="" placeholder="..." required="true">
            </div>
            <div class="form-group">
                <label for="">Pajak Hingga</label>
                <input type="text" class="form-control" name="pajak" id="" placeholder="..." required="true">
            </div>
            <div class="form-group">
                <label for="">Alamat Inspeksi</label>
                <textarea class="form-control" name="alamat" id="" cols="10" rows="5" required="true"></textarea>
            </div>
            <button type="submit" class="btn btn-primary btn-lg pull-right">Submit</button>
        </form>
    </div>
</div>
<?php $this->load->view('partials/footer'); ?>
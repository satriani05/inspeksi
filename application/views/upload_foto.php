<?php $this->load->view('partials/header');?>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h4>Upload Foto Inspeksi</h4>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-lg-12">
            <div class="dropzone">
              <div class="dz-message text-center">
                <h3> Klik atau Drop gambar disini</h3>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <a href="<?php echo site_url('inspeksi/edit_upload/'.$this->uri->segment(3));?>" class="btn btn-primary pull-right" style="margin-top:20px;">edit upload</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dropzone.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/basic.min.css') ?>">
<script src="<?php echo base_url(); ?>assets/js/dropzone.min.js"></script>
<script>
$(document).ready(function(){
  Dropzone.autoDiscover = false;
  var foto_upload= new Dropzone(".dropzone",{
  url: "<?php echo site_url('inspeksi/do_upload/'.$id_pesan) ?>",
  maxFilesize: 20,
  method:"post",
  acceptedFiles:"image/*",
  paramName:"fotoInspeksi",
  dictInvalidFileType:"Type file ini tidak dizinkan",
  addRemoveLinks:true,
  });


  //Event ketika Memulai mengupload
  foto_upload.on("sending",function(a,b,c){
    a.token=Math.random();
    c.append("token_foto",a.token); //Menmpersiapkan token untuk masing masing foto
  });


  // Event ketika foto dihapus
  foto_upload.on("removedfile",function(a){
    var token=a.token;
    $.ajax({
      type:"post",
      data:{token:token},
      url:"<?php echo site_url('inspeksi/remove_foto') ?>",
      cache:false,
      dataType: 'json',
      success: function(){
        console.log("Foto terhapus");
      },
      error: function(){
        console.log("Error");

      }
    });
  });
})
</script>
<style type="text/css">
.dropzone {
	margin-top: 50px;
	border: 2px dashed #0087F7;
}
</style>
<?php $this->load->view('partials/footer');
